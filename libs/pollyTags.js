const escape = /("|&|'|<|>)/g;

async function translate(prompt) {
  const translations = {
    break: { reg: /<break>/g, tag: "break/", end: /<[/]break>/g, close: "break" },
    emphasis: {
      reg: /<e-([smr])>/g,
      tag: "emphasis level=",
      s: "strong",
      m: "moderate",
      r: "reduced",
      end: /<[/]e>/g,
      close: "</emphasis>",
    },
    volume: { reg: /<v-(x-soft|soft|medium|loud|x-loud)>/g, tag: "prosody volume=", end: /<[/]v>/g, close: "</prosody>" },
    rate: { reg: /<r-(\d\d?\d)>/g, tag: "prosody rate=", end: /<[/]r>/g, close: "</prosody>" },
    pitch: { reg: /<p-(x-low|low|medium|high|x-high)>/g, tag: "prosody pitch=", end: /<[/]p>/g, close: "</prosody>" },
    pronunciation: {
      reg: /<pr-(verb|past|deter|prep|adj|noun)>/g,
      tag: "w role=",
      verb: "amazon:VB",
      past: "amazon:VBD",
      deter: "amazon:DT",
      prep: "amazon:IN",
      adj: "amazon:JJ",
      noun: "amazon:NN",
      end: /<[/]pr>/g,
      close: "</w>",
    },
    breath: { reg: /<breath>/g, tag: "amazon:breath/", end: /<[/]breath>/g, close: "breath" },
    // newscaster: { reg: /<news>/g, tag: 'amazon:domain name="news"', end: /<[/]news>/g, close: "</amazon:domain>" }, !NEURAL ONRY!
    softly: { reg: /<soft>/g, tag: 'amazon:effect phonation="soft"', end: /<[/]soft>/g, close: "</amazon:effect>" },
    whisper: { reg: /<wi>/g, tag: 'amazon:effect name="whispered"', end: /<[/]wi>/g, close: "</amazon:effect>" },
    tract: { reg: /<t-(\d\d?\d)>/g, tag: "amazon:effect vocal-tract-length=", end: /<[/]t>/g, close: "</amazon:effect>" },
  };
  var translated = prompt;
  for await (const [transl, value] of Object.entries(translations)) {
    console.log(transl);
    const getTags = prompt.matchAll(value.reg);
    console.log(getTags);
    if (value.reg.test(prompt) === false) {
      console.log("false");
      continue;
    }
    const tags = Array.from(getTags);
    console.log(tags);
    for await (const tag of tags){
      switch (transl) {
        case "break":
        case "breath": {
          var insert = "<" + value.tag + ">";
          translated = translated.replaceAll(tag[0], insert);
          translated = translated.replaceAll(value.end, "");
          break;
        }
        case "emphasis":
        case "pronunciation": {
          console.log(tag[0]);
          var option = value[tag[1]];
          option = "<" + value.tag + '"' + option + '"' + ">";
          console.log(option);
  
          translated = translated.replaceAll(tag[0], option);
          translated = translated.replaceAll(value.end, value.close);
          break;
        }
        case "rate":
        case "tract": {
          console.log(tag[0]);
          var option = tag[1];
          option = "<" + value.tag + '"' + option + '%"' + ">";
          console.log(option);
          translated = translated.replaceAll(tag[0], option);
          translated = translated.replaceAll(value.end, value.close);
          break;
        }
        case "volume":
        case "pitch": {
          console.log(tag[0]);
          var option = tag[1];
          option = "<" + value.tag + '"' + option + '"' + ">";
          console.log(option);
          translated = translated.replaceAll(tag[0], option);
          translated = translated.replaceAll(value.end, value.close);
          break;
        }
        case "newscaster":
        case "softly":
        case "whisper":
          var insert = "<" + value.tag + ">";
          translated = translated.replaceAll(tag[0], insert);
          translated = translated.replaceAll(value.end, value.close);
          break;
        default:
          translated = translated.replaceAll(tags[0], "");
          break;
      }
      console.log(translated);
    }
    }
  return translated;
}

module.exports = { translate };
