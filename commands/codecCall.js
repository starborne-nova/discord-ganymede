const { SlashCommandBuilder } = require('@discordjs/builders');
const { AttachmentBuilder } = require('discord.js');
const { createCanvas, loadImage, registerFont, Image } = require('canvas')
const path = require('node:path')
const codecTestS = /(Hnngh[.]{3}Colonel[.]{3})\s(.*)/
const codecTestC = /(Snake[.]{3})\s(.*)/
const codecTest = /(Hnngh[.]{3}Colonel[.]{3})\s(.*)|(Snake[.]{3})\s(.*)/
registerFont('./assets/fonts/Avenir.ttf', { family: 'Avenir' })
registerFont('./assets/fonts/Arial.ttf', { family: 'Arial' })


module.exports = {
    data: new SlashCommandBuilder()
        .setName('codec')
        .setDescription('Colonel...')
        .addStringOption(option => (
            option.setName('speaker')
                .setDescription('Choose who is speaking')
                .addChoices(
                    {name:'Snake speaking to Colonel', value:'Snake'},
                    {name:'Colonel speaking to Snake', value:'Colonel'}
                )
                .setRequired(true)))
        .addStringOption(option => (
            option.setName('message')
                .setDescription('Put ya words here')
                .setRequired(true))),
    async execute(interaction) {
        const speaker = interaction.options.getString('speaker')
        const inputText = interaction.options.getString('message')
        const text = speaker == 'Snake'?('Colonel, ' + inputText).split("").join(String.fromCharCode(8202)):('Snake, ' + inputText).split("").join(String.fromCharCode(8202))
          const canvas = new createCanvas(640, 439);
          const ctx = canvas.getContext('2d')
          const img = new Image()
          img.onload = () => ctx.drawImage(img, 0, 0)
          img.onerror = err => { throw err }
          img.src = path.join(__dirname ,'/images/codec.png')
          ctx.fillStyle = '#d3d3d3'
          ctx.font = '22px Arial'
          
          const processed = fragmentText(text, ctx, 375)
          const joined = processed.join('\n')
          ctx.fillText(joined, 150, 295, 375)
          const attachment = new AttachmentBuilder(canvas.toBuffer('image/png', { compressionLevel: 3, filters: canvas.PNG_FILTER_NONE }), { name: 'codec-image.png' });
          await interaction.reply({
            files: [attachment],
            content: 'Incoming message:'
          });
        }
        
    };

    function fragmentText(text, ctx, maxWidth) {
      var words = text.split(' '),
          lines = [],
          line = "";
      if (ctx.measureText(text).width < maxWidth) {
          return [text];
      }
      while (words.length > 0) {
          var split = false;
          while (ctx.measureText(words[0]).width >= maxWidth) {
              var tmp = words[0];
              words[0] = tmp.slice(0, -1);
              if (!split) {
                  split = true;
                  words.splice(1, 0, tmp.slice(-1));
              } else {
                  words[1] = tmp.slice(-1) + words[1];
              }
          }
          if (ctx.measureText(line + words[0]).width < maxWidth) {
              line += words.shift() + " ";
          } else {
              lines.push(line);
              line = "";
          }
          if (words.length === 0) {
              lines.push(line);
          }
      }
      return lines;
    }