const { SlashCommandBuilder } = require('@discordjs/builders');
const { AttachmentBuilder} = require('discord.js'); 

// What the fuck is discords issue with attaching audio
//Make it streaming audio IG
//Fucking piece of shit app

module.exports = {
    data: new SlashCommandBuilder()
        .setName('pollysample')
        .setDescription("Pull a sample for a voice")
        .addStringOption(option => (
                option.setName('voice')
                    .setDescription('Pick a voice')
                    .addChoices(
                        {name:'Brian - UK', value:'Brian'},
                        {name:'Amy - UK', value:'Amy'},
                        {name:'Emma - UK', value:'Emma'},
                        {name:'Ivy (Child)', value:'Ivy'},
                        {name:'Joanna', value:'Joanna'},
                        {name:'Kendra', value:'Kendra'},
                        {name:'Salli', value:'Salli'},
                        {name:'Joey', value:'Joey'},
                        {name:'Justin (Child)', value:'Justin'},
                        {name:'Matthew', value:'Matthew'},
                        {name:'Geraint - Welsh', value:'Geirant'}
                    )
                    .setRequired(true))),
            
    async execute(interaction) {
        const choice1 = interaction.options.getString('voice')
        const fpath = './assets/samples/' + choice1.toLowerCase() + 'Samp.mp3'
        
        
        await interaction.reply({content:'Here is a sample of ' + choice1, files: [fpath], ephemeral: true});
    },
};