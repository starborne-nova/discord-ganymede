const { SlashCommandBuilder } = require('@discordjs/builders')
const { EmbedBuilder } = require('discord.js')
const { logger } = require('../components/logger.js')
const findStamp = /\d\d:\d\d:\d\d/gm
const extractEmotes = /(:\w+:)/gm
const rankNames = ['First', 'Second', 'Third', 'Fourth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth']
var listType = ''

//Find a way to filter out those fucking timestamps
//Write a utility using Got to fetch all them messages, boy

function leaderboard (a, b) {
  return b[1] - a[1]
}

function loserboard (a, b) {
  return a[1] - b[1]
}

module.exports = {
  data: new SlashCommandBuilder()
    .setName('emojistats')
    .setDescription('Check the numbers')
    .addStringOption(option =>
      option.setName('ranking')
        .setDescription('How to rank emotes')
        .addChoices(
          { name: 'Most Used', value: 'Leaderboard' }, 
          { name: 'Least Used', value: 'Loserboard' }
        )
        .setRequired(true)
    )
    .addBooleanOption(option =>
      option.setName('custom')
        .setDescription('Limit to custom emotes')
        .setRequired(true)
    ),
  async execute (interaction) {
    try {
      if (interaction.user.id === '341018945501003776' || interaction.user.id === '341410029456326661') {
        // Only me and fox
        listType = interaction.options.getString('ranking')
        interaction.deferReply()
        console.log('Starting')
        const allChannels = await interaction.guild.channels.fetch()
        const emotes = {}
        for await (const channel of allChannels) {
          console.log('Channel Pass')
          const thisChannel = channel[1]
          if (thisChannel.type != 0 || !thisChannel.viewable) {
            logger.info('channel type = ' + thisChannel.type + '. Ignoring')
            continue
          }
          const messages = []
          // logger.debug(thisChannel)
          let message = await thisChannel.messages
            .fetch({ limit: 1 })
            .then(messagePage => (messagePage.size === 1 ? messagePage.at(0) : null))

          while (message) {
            console.log('Page Hit')
            await thisChannel.messages.fetch({ limit: 100, before: message.id }).then(messagePage => {
              messagePage.forEach(msg => {
                if (msg.content.length > 0) {
                  messages.push(msg)
                }
              })

              message = 0 < messagePage.size ? messagePage.at(messagePage.size - 1) : null
              if (messages.length >= 1000) {
                message = null
              }
            })
          }

          console.log('Messages Filtering')
          for await (const msg of messages) {
            let thisMessage = msg.content.replaceAll(findStamp, '')
            let filterMessage = thisMessage.match(extractEmotes)
            if (thisMessage.length != 0 && filterMessage != null) {
                for (const match of filterMessage) {
                  console.log(match)
                  var formatted = match.replaceAll(':', '')
                  var eSearch = interaction.client.emojis.cache.find(emoji => emoji.name === formatted)
                  if (eSearch === undefined) {
                    if (interaction.options.getBoolean('custom')) {
                      continue
                    }
                    formatted = match
                    var existing = emotes.hasOwnProperty(formatted)
                    if (!existing) {
                      Object.assign(emotes, { [formatted]: 1 })
                      continue
                    }
                    emotes[formatted]++
                  } else {
                    if (emotes.hasOwnProperty(eSearch) === false) {
                      Object.assign(emotes, { [eSearch]: 1 })
                      continue
                    }
                    emotes[eSearch]++
                  }
                  console.log(formatted)
                }
              
            }
          }

          //Getting caught in a hell loop here
        }
        console.log('Filter loop broken')
        const emoteArr = Object.entries(emotes)
        if(listType === 'leader'){
          emoteArr.sort(leaderboard)
        }
        else{
          emoteArr.sort(loserboard)
        }
        console.log(emoteArr)
        const boardEmbed = new EmbedBuilder()
        .setColor('#0099ff')
        .setTitle('Emotes Used')
        .setDescription(listType)
        let ind = 0

        while(ind < emoteArr.length && ind < 10) {
          console.log(ind)
          console.log(emoteArr[ind])
          boardEmbed.addFields({ name: rankNames[ind], value: emoteArr[ind][0] + ' used ' + emoteArr[ind][1].toString() + ' times' })
          ind++
        }
        console.log('cock')
        interaction.editReply({ embeds: [boardEmbed] })
      }
    } catch (e) {
      console.log(e)
    }
    //await interaction.reply("Check the logs, penis")
  }
}
//lol ik what this does

//What if you just wanted to fetch data but discord hid it behind 1 billion abstractions
