const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('help')
        .setDescription("Lists all of Ganymede's commands"),
    async execute(interaction) {
        const helpEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Ganymede Commands')
            .setDescription('how to request actions')
            .addFields(
                { name: '/today', value: "Posts today's memeday of the week" },
                { name: '/codec', value: 'Generate a codec call image' },
                { name: '/ping', value: 'Pings you to test latency'},
                { name: '/showtime', value: 'Posts the time that Ganymede lives in' },
                { name: '/chill', value: 'Puts a 12 hour sleep timer on responding to other bots'},
                { name: '/polly', value: 'Generates TTS using Amazon Polly'},
                { name: '/helpme', value: 'Inspiration that sometimes delivers a OHKO' },
                { name: '/fortune', value: 'Reads your fortune' },
                { name: '/pollyhelp', value: 'Modifier tags you can use in a Polly request'},
                { name: '/pollypronounce', value: 'Pronunciation tags to control individual words'},
                { name: '/pollysample', value: 'Get the sample of a selected voice'},
                { name: 'Make me [mealtime]', value: 'Generates a meal for you. Accepts bastardized versions and a few secret words' },
                { name: 'Temperature conversion', value: 'Convert <value> please' },
                { name: 'Role Picker', value: 'say ROLE in the role channel to bring up the menu' }
            )

        await interaction.reply({embeds:[helpEmbed], ephemeral: true});
    },
};