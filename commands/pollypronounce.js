const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('pollypronounce')
        .setDescription("Lists Special TTS Tags"),
    async execute(interaction) {
        const helpEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Polly Pronunciation Tags')
            .setAuthor({ name: 'Ganymede', iconURL: 'https://i.imgur.com/AfFp7pu.png' })
            .setDescription('control every single word')
            .setThumbnail('https://i.imgur.com/AfFp7pu.png')
            .addFields(
                { name: 'verb', value: '<pr-verb> Pronounces the word with a different inflection </pr>' },
                { name: 'past', value: '<pr-past> Past Tense </pr>'},
                { name: 'deter', value: '<pr-deter> Determinative, declarative </pr>' },
                { name: 'prep', value: '<pr-prep> Preposition </pr>' },
                { name: 'adj', value: '<pr-adj> Adjective </pr>'},
                { name: 'noun', value: '<pr-noun> Pronounces the word with a different inflection </pr>'},
            )

        await interaction.reply({embeds:[helpEmbed], ephemeral: true});
    },
};