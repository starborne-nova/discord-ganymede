const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('pollyhelp')
        .setDescription("Lists special TTS tags"),
    async execute(interaction) {
        const helpEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Polly Tags')
            .setAuthor({ name: 'Ganymede', iconURL: 'https://i.imgur.com/AfFp7pu.png' })
            .setDescription('how to do the TTS')
            .setThumbnail('https://i.imgur.com/AfFp7pu.png')
            .addFields(
                { name: 'Break', value: '<break>' },
                { name: 'Emphasis', value: '<e-s> Use (s)trong, (m)oderate, or (r)educed </e>'},
                { name: 'Volume', value: '<v-x-soft> Use x-soft, soft, medium, loud, x-loud </v>' },
                { name: 'Rate', value: '<r-50> Set the reading rate as a percentage of normal speed </r>' },
                { name: 'Pitch', value: '<p-x-low> Use x-low, low, medium, high, x-high </p>'},
                { name: 'Breath', value: '<breath> Use this to add a pause for inhaling </breath>'},
                { name: 'Soft', value: '<soft> Use this to make the syllables softer </soft>' },
                { name: 'Whisper', value: '<wi> Use this to whisper </wi>' },
                { name: 'Vocal Tract', value: '<t-50> Use this to set an absolute percentage of normal vocal tract length. Affects pitch and muddiness of the speech </t>' },
                { name: 'Individual Pronunciation', value: 'granular tags to change the inflection of words. /polly pronounce to list them' }  
            )

        await interaction.reply({embeds:[helpEmbed], ephemeral: true});
    },
};