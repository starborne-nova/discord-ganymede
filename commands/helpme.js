const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');
const fs = require('node:fs');
const path = require('node:path');
const randy = require('../components/randy.js')
const fortunePath = path.join(__dirname, "images/inspo")
const fortunes = fs.readdirSync(fortunePath)
const fortcount = fortunes.length

module.exports = {
    data:new SlashCommandBuilder()
    .setName('helpme')
    .setDescription("Delivers inspiration that is sometimes a one-hit KO."),
    async execute(interaction) {
        const number = Math.floor(fortcount * randy.randReal1());
        const file = "fortunes" + (number.toString()) + ".jpg";
        const yourFortune = new AttachmentBuilder('./commands/images/inspo/' + file);
        const fortuneEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle("Here's some inspiration:")
            .setAuthor({ name: 'Ganymede'})
            .setImage('attachment://' + file);
        console.log(fortunes);
        console.log(yourFortune);
        console.log(number);
        await interaction.reply({ embeds: [fortuneEmbed], files: [yourFortune] });
    }
}