const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('chill')
        .setDescription("Stops bot violence for 12 hours"),
    async execute(interaction) {
        const { cooldowns } = interaction.client
        const now = new Date()
        const buffer = 43200
        const timeout = buffer * 1_000
        const newTime = now + timeout
        cooldowns.set('chillout',newTime)
        await interaction.reply({content:'12hr timer has been reset', ephemeral: true});
    },
};