const { SlashCommandBuilder } = require('@discordjs/builders');
const { AttachmentBuilder, EmbedBuilder } = require('discord.js');
const monday = new AttachmentBuilder('./assets/images/monday.jpg');
const tuesday = new AttachmentBuilder('./assets/images/tuesday01.jpg');
const wednesday = new AttachmentBuilder('./assets/images/wednesday.jpg');
const thursday = new AttachmentBuilder('./assets/images/thursday.jpg');
const friday = new AttachmentBuilder('./assets/images/friday.jpg');
const saturday = new AttachmentBuilder('./assets/images/saturday.jpg');
const sunday = new AttachmentBuilder('./assets/images/sunday.gif');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('today')
		.setDescription('What day is it?'),
	async execute(interaction) {
		const findNow = new Date()
		const findToday = changeTimezone(findNow, "America/New_York")
		const dayNow = findToday.getDay();
		const dayEmbed = new EmbedBuilder()
			.setColor('#0099ff')
			.setAuthor({ name: 'Ganymede', iconURL: 'https://i.imgur.com/AfFp7pu.png' })
			.setThumbnail('https://i.imgur.com/AfFp7pu.png')
			.setTimestamp()
			.setFooter({ text: 'Hi, I like humans but other bots piss me off', iconURL: 'https://i.imgur.com/AfFp7pu.png' });

		dayEmbed.setTitle(interaction.user.username + " asked what day it is")

		switch (dayNow) {
			case 0:
				dayEmbed.setURL('https://www.youtube.com/watch?v=6F5azNTnaOI');
				dayEmbed.setDescription("Happy Fingers in His Ass Sunday!");
				dayEmbed.setImage('attachment://sunday.gif');
				await interaction.reply({ embeds: [dayEmbed], files: [sunday] });
				break;

			case 1:
				dayEmbed.setDescription("Happy Miku Monday!");
				dayEmbed.setImage('attachment://monday.jpg');
				await interaction.reply({ embeds: [dayEmbed], files: [monday] });
				break;

			case 2:
				dayEmbed.setDescription("Tuesday again? No problem...");
				dayEmbed.setImage('attachment://tuesday01.jpg');
				await interaction.reply({ embeds: [dayEmbed], files: [tuesday] });
				break;

			case 3:
				dayEmbed.setDescription("It is Wednesday my dudes!");
				dayEmbed.setImage('attachment://wednesday.jpg')
				await interaction.reply({ embeds: [dayEmbed], files: [wednesday] });
				break;

			case 4:
				dayEmbed.setURL('https://www.youtube.com/watch?v=d7xMgJedN2s');
				dayEmbed.setDescription("Happy Out of Touch Thursday!");
				dayEmbed.setImage('attachment://thursday.jpg')
				await interaction.reply({ embeds: [dayEmbed], files: [thursday] });
				break;

			case 5:
				dayEmbed.setDescription("Happy Flat Fuck Friday!");
				dayEmbed.setImage('attachment://friday.jpg')
				await interaction.reply({ embeds: [dayEmbed], files: [friday] });
				break;

			case 6:
				dayEmbed.setDescription("You have now entered: Radical Saturday!");
				dayEmbed.setImage('attachment://saturday.jpg')
				await interaction.reply({ embeds: [dayEmbed], files: [saturday] });
				break;
		}
	},
};

function changeTimezone(date, ianatz) {

	// suppose the date is 12:00 UTC
	var invdate = new Date(date.toLocaleString('en-US', {
		timeZone: ianatz
	}));
  
	// then invdate will be 07:00 in Toronto
	// and the diff is 5 hours
	var diff = date.getTime() - invdate.getTime();
  
	// so 12:00 in Toronto is 17:00 UTC
	return new Date(date.getTime() - diff); // needs to substract
  
  }


