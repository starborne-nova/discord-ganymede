const { PollyClient, SynthesizeSpeechCommand } = require("@aws-sdk/client-polly");
const REGION = "us-east-2";
const pollyClient = new PollyClient({ region: REGION });
const { SlashCommandBuilder } = require('@discordjs/builders');
const { translate } = require('../libs/pollyTags.js')

// Add options for long-form output/voices
// Add a safeguard to break prompts into 2000 character pieces

module.exports = {
    data: new SlashCommandBuilder()
        .setName('polly')
        .setDescription('Says the words you type')
        .addStringOption(option => (
            option.setName('speech')
                .setDescription('The words to say')
                .setRequired(true)))
        .addStringOption(option => (
            option.setName('voice')
                .setDescription('Pick a voice')
                .addChoices(
                    {name:'Brian - UK', value:'Brian'},
                    {name:'Amy - UK', value:'Amy'},
                    {name:'Emma - UK', value:'Emma'},
                    {name:'Ivy (Child)', value:'Ivy'},
                    {name:'Joanna', value:'Joanna'},
                    {name:'Kendra', value:'Kendra'},
                    {name:'Salli', value:'Salli'},
                    {name:'Joey', value:'Joey'},
                    {name:'Justin (Child)', value:'Justin'},
                    {name:'Matthew', value:'Matthew'},
                    {name:'Geraint - Welsh', value:'Geraint'}
                )
                .setRequired(true))),
    async execute(interaction) {
        const choice1 = interaction.options.getString('speech')
        const choice2 = interaction.options.getString('voice')
        const translator = await translate(choice1)
        console.log(translator)
        const payload = "<speak>" + translator + "</speak>"
        console.log(payload)
        
        const input = { // SynthesizeSpeechInput
            Engine: "standard",
            LanguageCode: "en-US",
            OutputFormat: "mp3", // required
            Text: payload, // required
            TextType: "ssml",
            VoiceId: choice2, // required
        };

         const command = new SynthesizeSpeechCommand(input);
         const response = await pollyClient.send(command);
        

        await interaction.reply(
            { content:("Voice used: " + choice2 + ". Your prompt was: " + choice1), files: [{ attachment: response.AudioStream, name: "pollyresponse.mp3" }], ephemeral: true }
        )
    },
};