const { SlashCommandBuilder } = require('@discordjs/builders');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('showtime')
		.setDescription('Replies with the time where the bot lives'),
	async execute(interaction) {
        const timeNow = new Date()
		await interaction.reply(timeNow.toUTCString());
	},
};