const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder, AttachmentBuilder } = require('discord.js');
const fs = require('node:fs');
const path = require('node:path');
const fortunes = require('../tables/fortunes.js')
const randy = require('../components/randy.js')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('fortune')
        .setDescription("Reads your fortune"),
    async execute(interaction) {
        const firstRoll = Math.floor(randy.randReal1() * 5);
        const rolledFortune = fortunes[firstRoll].length
        const secondRoll = Math.floor(randy.randReal1() * rolledFortune);
        const yourFortune = fortunes[firstRoll][secondRoll]
        const luckyNumber = Math.floor(randy.randReal1() * 100)
        console.log(firstRoll + ', ' + secondRoll)
        console.log(yourFortune);
        console.log(luckyNumber);
        const fortuneEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle("Here you go:")
            .setAuthor({ name: 'Ganymede'})
            .addFields(
                {name: 'Fortune', value: yourFortune},
                {name: 'Lucky Number', value: luckyNumber.toString()}
            );
        await interaction.reply({ embeds: [fortuneEmbed] });
    },
};