const { AttachmentBuilder } = require('discord.js');
const { logger } = require('../components/logger.js')
const meals = require("../tables/meals.js");
const mealtimes = require("../tables/mealtimes.js");
const searchMeal = /^make\sme/i;
const searchDessert = /with\sdessert/i;
const searchName = /(?<=me\s)\S+/i;
const randy = require('../components/randy.js')
const { Canvas, resolveImage } = require('canvas-constructor/napi-rs')
const { createCanvas, Image, loadImage, } = require('@napi-rs/canvas');
const { readFile } = require('fs/promises');
const { request } = require('undici');

//It would be funny to add fortune cookie fortunes
// Switch this shit to js-emoji because it's actually up to date

async function chef(message, avURL) {
  if (searchMeal.test(message.content)) {
    const mainL = meals.main.length
    const sideL = meals.side.length
    const drinkL = meals.drink.length
    const dessertL = meals.dessert.length
    const mealName = searchName.exec(message.content);
    const dessert = searchDessert.test(message.content);
    const mainRNG = Math.floor(randy.randReal1() * mainL.toString())
    const sideRNG =  Math.floor(randy.randReal1() * sideL.toString())
    const drinkRNG =  Math.floor(randy.randReal1() * drinkL.toString())
    const dessertRNG =  Math.floor(randy.randReal1() * dessertL.toString())
    var mealResponse = "";
    logger.debug(mainRNG);
    logger.debug(sideRNG);
    logger.debug(drinkRNG);
    if(mealName === null){
      return
    }
    Object.keys(mealtimes).forEach((mealtime) => {
      if (mealtimes[mealtime].includes(mealName[0].toLowerCase())) {
        mealResponse = mealtime;
        logger.info(mealResponse);
      } else if (message.content.includes("a sandwich")) {
        mealResponse = "sandwich";
      } else if (message.content.includes("a god") || message.content.includes("divine")) {
        mealResponse = "divine";
      }
      else if (message.content.includes("a pickle")) {
        mealResponse = "pickle";
      }
      else if (message.content.includes("simp")) {
        mealResponse = "simp";
      }
    });

    logger.debug(meals[mainRNG]);
    logger.debug(meals[sideRNG]);
    logger.debug(meals[drinkRNG]);

    switch (mealResponse) {
      case "meal":
        var output =
          "Your " +
          mealName[0] +
          " is " +
          meals.main[mainRNG].replaceAll('_', ' ').replaceAll(':', '') +
          " with a side of " +
          meals.side[sideRNG].replaceAll('_', ' ').replaceAll(':', '') +
          ", and " +
          meals.drink[drinkRNG].replaceAll('_', ' ').replaceAll(':', '') +
          " to drink. " +
          "\n" +
          meals.main[mainRNG] +
          meals.side[sideRNG] +
          meals.drink[drinkRNG];

        if (dessert) {
          var addDessert =
            "\nYour dessert is " +
            meals.dessert[dessertRNG].replaceAll('_', ' ').replaceAll(':', '') +
            "\n" +
            meals.dessert[dessertRNG];
          message.channel.send(output + addDessert);
        } else {
          message.channel.send(output);
        }
        break;
      case "dessert":
        message.channel.send("Your dessert is " + meals.dessert[dessertRNG].replaceAll('_', ' ').replaceAll(':', '') + "\n" + meals.dessert[dessertRNG])
        break;
      case "punish":
        message.channel.send(
          "Make you " + mealName[0] + "? Why don't you " + mealName[0] + " back when you have a better joke, smartass"
        );
        break;
      case "sandwich":
        message.channel.send("Alright, " + message.author.toString() + ", You're now a sandwich!");
        break;
      case "divine":
        message.channel.send("Wrong, pregnant man instead! " + '\:pregnant_man:\:pregnant_man:\:pregnant_man:');
        break;
      case "pickle": {
        const canvas = new Canvas(600, 796);

        const background = await loadImage('./assets/images/pickle.png');
        canvas.printImage(background, 0, 0, background.width, background.height);
        const avatar = await loadImage(avURL)

        canvas.printCircularImage(avatar, 435, 164, (avatar.width / 2));

        const attachment = new AttachmentBuilder(canvas.png(), { name: 'profile-image.png' });
        message.channel.send({
          files: [attachment],
          content: 'I turned ' + message.author.toString() + ' into a pickle. Funniest shit I\'ve ever seen'
        });
      }
        break;
        case "shrimp": {
          const canvas = new Canvas(600, 400);
  
          const background = await loadImage('./assets/images/shrimp.png');
          canvas.printImage(background, 0, 0, background.width, background.height);
          const avatar = await loadImage(avURL)
  
          canvas.printCircularImage(avatar, 325, 137, (avatar.width / 6));
  
          const attachment = new AttachmentBuilder(canvas.png(), { name: 'profile-image.png' });
          message.channel.send({
            files: [attachment],
            content: 'Not so shrimple now, is it, ' + message.author.toString() + '?'
          });
        }
          break;
      case "simp": {
        const canvas = new Canvas(600, 797);

        const background = await loadImage('./assets/images/simp.png');
        canvas.printImage(background, 0, 0, background.width, background.height);
        const avatar = await loadImage(avURL)

        canvas.printCircularImage(avatar, 297, 548, (avatar.width / 2));

        const attachment = new AttachmentBuilder(canvas.png(), { name: 'profile-image.png' });
        message.channel.send({
          files: [attachment],
          content: message.author.toString() + ' has been caught simping again...'
        });
      }
        break;
      case "happy":{
        const happRNG = Math.floor(randy.randReal1() * (meals.happy.length))
        message.channel.send(message.author.toString() + ', I mean this with all the love a cold unfeeling machine can give: ' + meals.happy[happRNG])
      }
      break;
      default:
        message.channel.send("I don't know how to do that.");
        break;
    }
  }
}

module.exports = { chef };
