const { createCanvas, loadImage, registerFont, Image } = require('canvas')
const { AttachmentBuilder } = require('discord.js');
const path = require('node:path')
const codecTestS = /(Hnngh[.]{3}Colonel[.]{3})\s(.*)/
const codecTestC = /(Snake[.]{3})\s(.*)/
const codecTest = /(Hnngh[.]{3}Colonel[.]{3})\s(.*)|(Snake[.]{3})\s(.*)/
registerFont('./assets/fonts/Avenir.ttf', { family: 'Avenir' })
registerFont('./assets/fonts/Arial.ttf', { family: 'Arial' })
//How the fuck do fonts work
//download and load your own fucking font I guess

//Literally paste the fonts and try to load them? IDK it's being fucking stupid
//Also look up how to install fonts on docker images

async function sendCodec(message, speaker) {

const scanner = speaker == 'Snake'?codecTestS.exec(message.content):codecTestC.exec(message.content)
const text = speaker == 'Snake'?('Colonel, ' + scanner[2]).split("").join(String.fromCharCode(8202)):('Snake, ' + scanner[2]).split("").join(String.fromCharCode(8202))
console.log(scanner)
  const canvas = new createCanvas(640, 439);
  const ctx = canvas.getContext('2d')
  const img = new Image()
  img.onload = () => ctx.drawImage(img, 0, 0)
  img.onerror = err => { throw err }
  img.src = path.join(__dirname ,'/images/codec.png')
  ctx.fillStyle = '#d3d3d3'
  ctx.font = '22px Arial'
  
  const processed = fragmentText(text, ctx, 375)
  const joined = processed.join('\n')
  ctx.fillText(joined, 150, 295, 375)
  const attachment = new AttachmentBuilder(canvas.toBuffer('image/png', { compressionLevel: 3, filters: canvas.PNG_FILTER_NONE }), { name: 'codec-image.png' });
  return await message.channel.send({
    files: [attachment],
    content: 'Incoming message:'
  });
}

function fragmentText(text, ctx, maxWidth) {
  var words = text.split(' '),
      lines = [],
      line = "";
  if (ctx.measureText(text).width < maxWidth) {
      return [text];
  }
  while (words.length > 0) {
      var split = false;
      while (ctx.measureText(words[0]).width >= maxWidth) {
          var tmp = words[0];
          words[0] = tmp.slice(0, -1);
          if (!split) {
              split = true;
              words.splice(1, 0, tmp.slice(-1));
          } else {
              words[1] = tmp.slice(-1) + words[1];
          }
      }
      if (ctx.measureText(line + words[0]).width < maxWidth) {
          line += words.shift() + " ";
      } else {
          lines.push(line);
          line = "";
      }
      if (words.length === 0) {
          lines.push(line);
      }
  }
  return lines;
}

module.exports = { sendCodec, codecTest, codecTestS, codecTestC }