const {logger} = require('../components/logger.js')
const { Events, ActionRowBuilder, StringSelectMenuBuilder, StringSelectMenuOptionBuilder } = require("discord.js");

async function roleIt(message){
   
  if(message.content === "ROLE"){
    try{
        await message.author.fetch()
        await message.guild.fetch()
        logger.info("role tripped")
        const select = new StringSelectMenuBuilder()
        .setCustomId("setRole")
        .setPlaceholder("Which role did you want to toggle?")
        const roleScan = await message.guild.roles.fetch()
        roleScan.forEach((role)=>{
          if(role.name.startsWith('!')){
            select.addOptions(
              new StringSelectMenuOptionBuilder()
              .setLabel(role.name)
              .setValue(role.name))
            }
          })
          
        // .addOptions(
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Minecraft")
        //     .setDescription("Minedcraft???")
        //     .setValue("Minecraft"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Lethal Company")
        //     .setDescription("YIPPEE")
        //     .setValue("Lethal Company"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Terraria")
        //     .setDescription("You leave that nurse alone")
        //     .setValue("Terraria"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Mario Party")
        //     .setDescription("Put your friendship to the test")
        //     .setValue("Mario Party"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Deep Rock Galactic")
        //     .setDescription("ROCK AND STONE!")
        //     .setValue("Deep Rock Galactic"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Puzzle Time")
        //     .setDescription("Puzzles and chill times")
        //     .setValue("Puzzle Time"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Movie and Chill")
        //     .setDescription("Come watch some stuff")
        //     .setValue("Movie and Chill"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Monster Hugger")
        //     .setDescription("Don't forget cool drinks")
        //     .setValue("Monster Hugger"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Helldivers")
        //     .setDescription("Where we droppin', boys?")
        //     .setValue("Helldivers"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Any MP Game")
        //     .setDescription("Get lots of pings")
        //     .setValue("Any MP Game"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("VR Chat")
        //     .setDescription("It's like real life, but virtual")
        //     .setValue("VR Chat"),
        //     new StringSelectMenuOptionBuilder()
        //     .setLabel("Just Kidding")
        //     .setDescription("I panicked")
        //     .setValue("jk")
        //     // new StringSelectMenuOptionBuilder()
        //     // .setLabel("Debug")
        //     // .setDescription("Penis")
        //     // .setValue("test")
        //   );
        const row = new ActionRowBuilder().addComponents(select);
        
        await message.reply({
          content: `Pick a role, ${message.author}!`,
          components: [row],
        });

        await message.delete()
        
        // const collectorFilter = i => i.user.id === interaction.user.id;
        
        // try {
        //   const confirmation = await response.awaitMessageComponent({ filter: collectorFilter, time: 60_000 });
        //   if (confirmation.customId === 'setRole') {
        //     //await interaction.guild.members.ban(target);
        //     await confirmation.update({ content: `${user} has chosen.`, components: [] });
        //   }
        // } catch (e) {
        //   await interaction.editReply({ content: 'Confirmation not received within 1 minute, cancelling', components: [] });
        // }
        }catch(e){
          logger.error(e)
        }}
  }

  module.exports = {roleIt}