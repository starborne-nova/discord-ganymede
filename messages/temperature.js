const convert = /(?:\s)(\d{1,3})\s?(C|F)\s?[.!_]/
const temp = /\s\d{1,3}\s?(?:C\s|F\s)/
const captureCels = /(?:\s)(\d{1,3})\s?(?:C)/
const captureFarh = /(?:\s)(\d{1,3})\s?(?:F)/

async function conversion(message){
    if(temp.test(message.content)){
        switch(captureCels.test(message.content)){
            case false:{
                const raw = message.content.match(captureFarh)
                console.log(raw)
                const conversion = Math.floor(((parseInt(raw[1])) - 32) / 1.8);
                message.reply("That's " + conversion + " in Celsius.")
            }
                break;
            case true:{
                const raw = message.content.match(captureCels)
                console.log(raw)
                const conversion = Math.floor(((parseInt(raw[1])) * 1.8) + 32);
                message.reply("That's " + conversion + " in American (F)reedom Units.")
            }
                break;

            default:{
                console.log("No Match")
            }
            break;

        }
    }
    else if(convert.test(message.content)){
        const raw = message.content.match(convert)
        switch(raw[2] === 'C'){
            case false:{
                const raw = message.content.match(convert)
                console.log(raw)
                const conversion = Math.floor(((parseInt(raw[1])) - 32) / 1.8);
                message.reply("That's " + conversion + " in Celsius.")
            }
                break;
            case true:{
                const raw = message.content.match(captureCels)
                console.log(raw)
                const conversion = Math.floor(((parseInt(raw[1])) * 1.8) + 32);
                message.reply("That's " + conversion + " in American (F)reedom Units.")
            }
                break;

            default:{
                console.log("No Match")
            }
            break;   
    }
    }
}

module.exports= {conversion}