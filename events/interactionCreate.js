const {logger} = require('../components/logger.js')
const { Events, PermissionsBitField } = require("discord.js");

module.exports = {
  name: Events.InteractionCreate,
  async execute(interaction) {
    const channel = interaction.client.channels.cache.get("1252084382341136436")
    try{
    if (interaction.isChatInputCommand()) {
      const command = interaction.client.commands.get(interaction.commandName);
        await command.execute(interaction); 
      
    } else if (interaction.isStringSelectMenu() && interaction.customId === "setRole") {
      const perms = interaction.guild.members.me.permissions.has(PermissionsBitField.Flags.ManageRoles)
      const userPerms = interaction.member.manageable
      logger.info(perms + " " + userPerms)
      if (interaction.values[0] === "jk") {
        await interaction.message.fetch();
        interaction.message.delete();
        return;
      }
      if(!perms){
        await interaction.message.fetch();
        interaction.message.delete();
        return;
      }
    
		const serverRoles = await interaction.member.guild.roles.fetch()
		const roleValue = interaction.values[0]
        let existing = false;
        interaction.member.roles.cache.forEach((role) => {
          if (roleValue === role.name) {
            interaction.member.roles.remove(role);
            existing = true;
          }
        });
        if (existing) {
		
          await interaction.message.fetch();
          logger.debug(interaction.message.content);
          interaction.message.delete();
          return;
        }
		serverRoles.forEach((role)=>{
			if(roleValue === role.name){
				interaction.member.roles.add(role)
			}
		})
        
		await interaction.message.fetch();
        logger.debug(interaction.message.content);
        interaction.message.delete();
      } else if (!interaction.isChatInputCommand()) {
        logger.info("Not Relevant");
        return;
      }
    }catch(e) {
      channel.send(e)
      .then(
        logger.error(`Error executing ${interaction.commandName} ` + e)
      );
    }
  },
};
