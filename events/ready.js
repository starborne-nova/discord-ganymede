const {logger} = require('../components/logger.js')
const { Events } = require('discord.js');
const meals = require('../tables/meals.js')

module.exports = {
	name: Events.ClientReady,
	once: true,
	execute(client) {
		logger.info(`Ready! Logged in as ${client.user.tag}`);
		const channel = client.channels.cache.get("1252084382341136436")
		const now = new Date().toString()
      channel.send(now + ": I'm Alive! I'm born!")
	}
};