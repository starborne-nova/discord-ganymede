const {logger} = require('../components/logger.js')
const { Events, Collection, PermissionsBitField } = require('discord.js');
const makeMeal = require('../messages/makeMeal.js')
const temperature = require('../messages/temperature.js')
const roleMaster = require('../messages/roleMastertext.js')
const randy = require('../components/randy.js')

//Make this a big switch instead of 100 if's


module.exports = {
    name: Events.MessageCreate,
    cooldown: 10,
    execute(message) {
        const channel = message.client.channels.cache.get("1252084382341136436")
        try{

        
        const perms = message.channel.permissionsFor(message.guild.members.me)
        const object = perms.serialize()
        if(object.SendMessages === false){
            logger.debug('Missing Message Permission')
            return
        }
        if(message.content.includes('PermCheck')){
            logger.info('BOT PERMS')
            logger.info(object)
        }
        if(message.content.includes('PermCheck+ME')){
            var you = message.channel.permissionsFor(message.author)
            var youperms = you.serialize()
            var youOut = you.toArray()
            logger.info("AUTHOR PERMS")
            logger.info(youperms)
            message.reply({content: (youOut.join(',\n '))}).then(sentMsg => {
                setTimeout(()=>{sentMsg.delete()}, 30000)
            })
        }
        if(message.content.includes('ROLLTIDE+')){
            for(let i=0; i < 10; i++){
                const roll = Math.floor(randy.randReal1() * 5);
                channel.send(roll.toString())
            }
        }
        makeMeal.chef(message, message.author.displayAvatarURL({ format: 'jpg' }));
        temperature.conversion(message)
        roleMaster.roleIt(message)
        
        //UNCOMMENT TO HARASS OTHER BOTS

        // if(message.author.bot && message.author.username != message.client.user.username){
        //     const { cooldowns } = message.client;

        //     if(cooldowns.has('chillout')){

        //     }

        //     if(!cooldowns.has(message.author)){
        //         cooldowns.set(message.author, new Collection());
        //     }

        //     const now = Date.now();
        //     const timestamps = cooldowns.get(message.author);
        //     const defaultCooldownDuration = 7200;
        //     const cooldownAmount = defaultCooldownDuration * 1_000;

        //     if(cooldowns.has('chillout')){
        //         const chilled = cooldowns.get('chillout')
        //         if(now < chilled){
        //             logger.info('Still Chillin')
        //             return;
        //         }
        //     }

        //     if (timestamps.has(message.author)) {
        //         const expirationTime = timestamps.get(message.author) + cooldownAmount;
            
        //         if (now < expirationTime) {
        //             logger.debug("too soon to mock")
        //             return
        //         }
        //     }
        //     const tableL = Object.keys(insultTable).length - 1
        //     const roll1 = randy.next(Math.floor(Math.random() * tableL.toString()), PRNG.mulberry32);
        //     const rand = Math.floor(roll1 * tableL);
        //     timestamps.set(message.author, now);
        //     setTimeout(() => timestamps.delete(message.author), cooldownAmount);
        // 	const targeted = message.author.toString()
        // 	logger.debug(rand)

        // 	message.reply(targeted + ", " + insultTable[rand])
        // }
    }catch(e){
        channel.send(e)
        .then(
            logger.error('Error executing ' + message.content + e)
        );
    }},
};
