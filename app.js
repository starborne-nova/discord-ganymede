const {logger} = require('./components/logger.js')
const fs = require('node:fs');
const path = require('node:path');
const { io } = require("socket.io-client");
const dotenv = require('dotenv').config()
const { Client, GatewayIntentBits, Partials, Collection, Events, PresenceUpdateStatus } = require('discord.js');

/* eslint-disable no-unused-vars */
// const schedule = require('node-schedule');
// const memeday = require('./messages/memeday.js')
const { token, streamers } = require('./config.json');
const insults = require('./tables/insults.js')
const stream = require('./components/stream.js')
//const notifManager  = require('./components/notifManager.js')
logger.debug(streamers)
const twitchData = {}


//Shit to fix:
//Can't read the ellipses character that mobile keyboards autocorrect to
//Rewrite in ESM
// Make a "make me friends" command that pastes their pfp on the friends logo



var lastEmbed = new Date()


const client = new Client({ intents: [
	GatewayIntentBits.Guilds,
	GatewayIntentBits.GuildMessages,
	GatewayIntentBits.MessageContent,
	GatewayIntentBits.GuildMembers,
	GatewayIntentBits.GuildMessageReactions
], partials: [Partials.Channel, Partials.Message, Partials.Reaction] });
client.commands = new Collection();
client.cooldowns = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

//const socket = io("http://localhost:3030")
const socket = io("https://star-reactor.fly.dev");

socket.on("connect", ()=>{
	logger.info('Websocket connect = ' + socket.connected);
	
	// true
});
socket.on("RESPONSE", (payload)=>{
console.log(payload)
})
const testing = socket.emit('TEST')

socket.on("Online", (payload, name) => {
	const goLive = name
	logger.info(payload + name)
	//notifManager.sendNotif(goLive, client)
	logger.info("Online update for " + name + " received")
	if(streamers.includes(name.toLowerCase())){
		//Calculate the minutes between the last embed and now
		const timeNow = new Date()
		const buffer = Math.abs(lastEmbed - timeNow)
		const lastEmbedMinutes = Math.ceil(buffer / (1000 * 60));
		//If it's been less than 5 minutes, do not send another embed
		if (lastEmbedMinutes < 5) {
			return;
		}
		//Update last embed time and send goLive. This is to stop embed spam from troubleshooting a stream.
		lastEmbed = new Date();
		const embed = stream.buildEmbed(payload, name)
		client.channels.cache.get("990809778576039966").send({ embeds: [embed] })
	}
	else{
		logger.info("Not on followed streamers")
		return
	}
})

socket.on("offline", (streamer) => {
	logger.info(streamer + " is Offline")
})

socket.on("test", (data) => {
	client.channels.cache.get("990809778576039966").send(data)
})

socket.on("disconnect", () => {
	logger.info(socket.connected); // false
});


for (const file of commandFiles) {
	const filePath = path.join(commandsPath, file);
	const command = require(filePath);
	// Set a new item in the Collection
	// With the key as the command name and the value as the exported module

	// VERY IMPORTANT
	// VERY IMPORTANT
	// VERY IMPORTANT
	// VERY IMPORTANT
	// VERY IMPORTANT
	// VERY IMPORTANT

	//Make a way to filter out joke roles for the role selector

	client.commands.set(command.data.name, command);
}

for (const file of eventFiles) {
	const filePath = path.join(eventsPath, file);
	const event = require(filePath);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}

client.login(token);
client.once(Events.ClientReady, readyClient => {
	client.user.setActivity('with your emotions')
});


// const memedays = new schedule.RecurrenceRule();
// memedays.hour = 12;
// memedays.minute = 0;
// memedays.tz = "America/New_York";

// const memedaysJob = schedule.scheduleJob(memedays, function(){
//   memeday.findDay(client)
// });


