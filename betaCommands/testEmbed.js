const { SlashCommandBuilder } = require('@discordjs/builders');
const { EmbedBuilder } = require('discord.js');


module.exports = {
    data: new SlashCommandBuilder()
        .setName('embed')
        .setDescription("Test embeds"),
    async execute(interaction) {
        const streamEmbed = new EmbedBuilder()
            .setColor(0x8053da)
            .setTitle("DesertP - Twitch")
            .setURL("https://twitch.tv/desertp")
            .setAuthor({ name: 'Twitch Alerts' })
            .addFields(
                { name: "Desert's words of wisdom:", value: "title" }
            )
            .setDescription('Desert is playing ' + "game")
            .setTimestamp()

        await interaction.reply({ embeds: [streamEmbed], ephemeral: true });
    },
};