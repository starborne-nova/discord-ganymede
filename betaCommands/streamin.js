const { SlashCommandBuilder } = require("@discordjs/builders");
const notifManager = require("../components/notifManager.js");
const axios = require("axios");

// AYYY LMAO add axios to check if it's a real twitch user

module.exports = {
  data: new SlashCommandBuilder()
    .setName("streaming")
    .setDescription("Sends you a DM when a streamer you want goes live")
    .addStringOption((option) =>
      option.setName("streamer").setDescription("The username of the streamer to send notifs").setRequired(true)
    ),
  async execute(interaction) {
    try {
      await interaction.deferReply({ ephemeral: true });
      const streamer = interaction.options.getString("streamer").toLowerCase();
      const uID = interaction.user.id.toString();
      const user = interaction.user.username;

      if (streamer === "oh my god help debug") {
        const penis = await notifManager.fuckYou();
        await interaction.editReply("Way to go, penis");
        return;
      }
      const usercheck = await axios.post(
        "https://star-reactor.fly.dev/usercheck",
        { data: [streamer] },
        {
          headers: {
            chrome: "stealthystars",
          },
        }
      );
      console.log(usercheck.data.data[0]);
      if (usercheck.status === 203) {
        const send = await interaction.editReply("Invalid Username");
        return;
      } else if (usercheck.data.data[0]) {
        const save = await notifManager.registerNotif(user, uID, streamer);
        console.log(save);
        await interaction.editReply("Registered " + streamer + " notifications for " + user);
        return;
      } else if (!usercheck.data.data[0].subscribed) {
        const activate = await axios.post(
          "https://star-reactor.fly.dev/userActivate",
          { data: [streamer] },
          {
            headers: {
              chrome: "stealthystars",
            },
          }
        );
        const activeCheck = await activate.toJSON();
        if (activeCheck.data[0] === "202%202%202") {
          const success = "success";
          const save = await notifManager.registerNotif(user, uID, streamer);
          console.log(save);
          await interaction.editReply("Registered " + streamer + " notifications for " + user);
          return;
        }
        await interaction.editReply("There was an error registering streamer");
        return;
      }
    } catch (e) {
      console.log(e);
    }
  },
};
