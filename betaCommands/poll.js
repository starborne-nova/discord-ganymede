/* eslint-disable no-unused-vars */
const { SlashCommandBuilder } = require('@discordjs/builders');
const { AttachmentBuilder, EmbedBuilder } = require('discord.js');
const emoji = require('node-emoji')

module.exports = {
    data: new SlashCommandBuilder()
        .setName('poll')
        .setDescription('Create a timed poll using reactions')
        .addStringOption(option => (
            option.setName('c1')
                .setDescription('The first poll choice')
                .setRequired(true)))
        .addStringOption(option => (
            option.setName('c2')
                .setDescription('The second poll choice')
                .setRequired(true)))
        .addStringOption(option => (
            option.setName('c3')
                .setDescription('The optional third poll choice')
        ))
        .addStringOption(option => (
            option.setName('c4')
                .setDescription('The optional fourth poll choice')
        ))

    ,
    async execute(interaction) {

        const choice1 = interaction.options.getString('c1')
        const choice2 = interaction.options.getString('c2')
        const timer = 60_000
       
        const pollEmbed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle(interaction.user.username + ' has created a poll!')
            .setAuthor({ name: 'Ganymede', iconURL: 'https://i.imgur.com/AfFp7pu.png' })
            .setDescription('Use the ' + emoji.get('one') + " and " + emoji.get('two') + ' reactions to vote!')
            .setThumbnail('https://i.imgur.com/AfFp7pu.png')
            .addFields(
                { name: 'Choice 1', value: choice1 },
                { name: 'Choice 2', value: choice2 }
            )
        if (interaction.options.getString('c3') != undefined) {
            pollEmbed.addFields({name:'Choice 3', value:interaction.options.getString('c3')})
            pollEmbed.setDescription('Use the ' + emoji.get('one') + ' , ' + emoji.get('two') + ' , and ' + emoji.get('three') + ' reactions to vote!')
        }
        if (interaction.options.getString('c4') != undefined) {
            pollEmbed.addFields({name:'Choice 4', value:interaction.options.getString('c4')})
            pollEmbed.setDescription('Use the ' + emoji.get('one') + ' , ' + emoji.get('two') + ' , ' + emoji.get('three') + ' , and ' + emoji.get('four') + ' reactions to vote!')
        }
        pollEmbed.addFields({name:'Poll Time', value:timer.toString() + ' minutes'})
        pollEmbed.setFooter({ text: 'Hi, I like humans but other bots piss me off', iconURL: 'https://i.imgur.com/AfFp7pu.png' });

        await interaction.reply({ embeds: [pollEmbed] })
        await interaction.fetchReply()
            .then(reply => {
                const filter = (reaction) => {
                    return reaction.emoji.name === emoji.get('one') | reaction.emoji.name === emoji.get('two') | reaction.emoji.name === emoji.get('three') | reaction.emoji.name === emoji.get('four')
                }
                reply.awaitReactions({ filter, time: timer })
                    .then(collected => {
                        const pollDone = reply.embeds[0]
                        const results = []
                        collected.map(value => {
                            console.log(emoji.replace(value._emoji.name, (emoji) => emoji.key))
                            switch (emoji.replace(value._emoji.name, (emoji) => emoji.key)) {
                                case 'one':
                                    console.log(value.count)
                                    results.push({ name: choice1, value: value.count + " votes" })

                                    break;
                                case 'two':
                                    console.log(value.count)
                                    results.push({ name: choice2, value: value.count + " votes" })

                                    break;
                                case 'three':
                                    if(pollDone.fields[2].name === 'Choice 3'){
                                        console.log(value.count)
                                        results.push({ name: pollDone.fields[2].value, value: value.count + " votes" })
                                    }

                                    break;
                                case 'four':
                                    if(pollDone.fields[2].name === 'Choice 4'){
                                        console.log(value.count)
                                        results.push({ name: pollDone.fields[3].value, value: value.count + " votes" })
                                    }

                                    break;
                                default:
                                    console.log('Item Skipped')

                                    break;
                            }
                        })
                        if(results.length === 1 && results.some(element => element.name === pollDone.fields[0].value) != true){
                            results.splice(0, 0, {name: pollDone.fields[0].value, value: "0 Votes"})
                        }
                        if(results.length === 1 && results.some(element => element.name === pollDone.fields[1].value) != true){
                            results.splice(1, 0, {name: pollDone.fields[1].value, value: "0 Votes"})
                        }
                        if(pollDone.fields.some(element=> element.name === 'Choice 3') && results.length === 2){
                            results.push({name: pollDone.fields[2].value, value: "0 Votes"})
                        }
                        if(pollDone.fields.some(element=> element.name === 'Choice 4') && results.length === 3){
                            results.push({name: pollDone.fields[3].value, value: "0 Votes"})
                        }
                        pollDone.setFields(results)
                        pollDone.setDescription('The poll is now closed')
                        interaction.editReply({ embeds: [pollDone] })
                    })
            })
    }
}