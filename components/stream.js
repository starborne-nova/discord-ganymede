const { EmbedBuilder } = require('discord.js');


function buildEmbed(streamer, name){
    const streamEmbed = new EmbedBuilder()
    .setColor(0x8053da)
    .setTitle(name + " - Twitch")
    .setURL("https://twitch.tv/" + name)
    .setAuthor({name:'Twitch Alerts'})
    streamEmbed.setThumbnail(streamer.profile)
    streamEmbed.setDescription('Playing ' + streamer.game)
    streamEmbed.addFields(
        {name: name + "'s words of wisdom:", value: streamer.title}
    )
    streamEmbed.setTimestamp()

    return streamEmbed
}

module.exports={buildEmbed}