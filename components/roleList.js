const roleSelect = {
}


async function getRoles(interaction){
    await interaction.member.guild.fetch()
    interaction.member.guild.roles.forEach((role)=>{
        const insert = {
            [role.name]: role
        }
        Object.assign(roleSelect, insert)
    })
    return roleSelect
}


module.exports = {getRoles}