const pino = require("pino");
const path = require("path");

const transports = [
    {
      target: 'pino/file',
      options: { destination: 1 } // this writes to STDOUT
    },
    {
      target: 'pino/file',
      options: { destination: __dirname + '/logs/devlog.log', mkdir: true } 
    }
  ]

// const logger = pino({
//   transport: {
//     pipeline: [{
//       target: './filePipeline.js'
//     }, {
//       // Use target: 'pino/file' with STDOUT descriptor 1 to write
//       // logs without any change.
//       target: 'pino/file',
//       options: { destination: __dirname + '/logs', mkdir: true}
//     }]
//   }
// })

const logger = pino(pino.transport({targets: transports}))

logger.info('Logger Started')

module.exports = {logger};